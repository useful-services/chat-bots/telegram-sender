package main_process

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"time"

	tg "gitlab.com/useful-services/chat-bots/telegram-sender/modules/telegram/services"
	ls "gitlab.com/useful-services/common/logger/constants"
	lg "gitlab.com/useful-services/common/logger/services"
)

var (
	mainProcess *MainProcess
)

func init() {
	mainProcess = newMainProcess()
}

func Process() *MainProcess {
	return mainProcess
}

type MainProcess struct {
	logger *lg.Logger
	server *http.Server
	router *gin.Engine
}

func (m *MainProcess) AddRoute() {

}

func (m *MainProcess) ListenAndServe() {
	tgService := tg.NewTelegramService(
		"https://api.telegram.org/bot%s/%s",
		viper.GetStringMapString("telegram")["token"],
		viper.GetStringMapString("telegram")["host"],
		m.logger,
	)

	defer tgService.Stop()
	err := m.server.ListenAndServe()
	if err != nil {
		m.logger.Error(err)
	}
}

func newMainProcess() *MainProcess {
	viper.SetConfigName("application-local")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("configurations/")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	logger := lg.NewLogger(ls.Default|ls.Info|ls.Debug, os.Stdout) // TODO config

	ReadTimeout, err := time.ParseDuration(viper.GetStringMapString("app")["read-timeout"])
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	WriteTimeout, err := time.ParseDuration(viper.GetStringMapString("app")["write-timeout"])
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	Addr := fmt.Sprintf(
		"%s:%s",
		viper.GetStringMapString("app")["host"],
		viper.GetStringMapString("app")["port"],
	)

	router := gin.Default()
	server := &http.Server{
		Addr:           Addr,
		Handler:        router,
		ReadTimeout:    ReadTimeout,
		WriteTimeout:   WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	return &MainProcess{
		logger: logger,
		server: server,
		router: router,
	}
}
