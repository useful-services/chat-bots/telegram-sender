package entities

type Message struct {
	MessageId            *int64   `json:"message_id"`                        // Unique message identifier inside this chat
	MessageThreadId      *int64   `json:"message_thread_id,omitempty"`       // Optional. Unique identifier of a message thread to which the message belongs; for supergroups only
	From                 *User    `json:"from,omitempty"`                    // Optional. Sender of the message; empty for messages sent to channels. For backward compatibility, the field contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
	SenderChat           *Chat    `json:"sender_chat,omitempty"`             // Optional. Sender of the message, sent on behalf of a chat. For example, the channel itself for channel posts, the supergroup itself for messages from anonymous group administrators, the linked channel for messages automatically forwarded to the discussion group. For backward compatibility, the field from contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
	Date                 *int64   `json:"date"`                              // Date the message was sent in Unix time
	Chat                 *Chat    `json:"chat"`                              // Conversation the message belongs to
	ForwardFrom          *User    `json:"forward_from,omitempty"`            // Optional. For forwarded messages, sender of the original message
	ForwardFromChat      *Chat    `json:"forward_from_chat,omitempty"`       // Optional. For messages forwarded from channels or from anonymous administrators, information about the original sender chat
	ForwardFromMessageId *int64   `json:"forward_from_message_id,omitempty"` // Optional. For messages forwarded from channels, identifier of the original message in the channel
	ForwardSignature     *string  `json:"forward_signature,omitempty"`       // Optional. For forwarded messages that were originally sent in channels or by an anonymous chat administrator, signature of the message sender if present
	ForwardSenderName    *string  `json:"forward_sender_name,omitempty"`     // Optional. Sender's name for messages forwarded from users who disallow adding a link to their account in forwarded messages
	ForwardDate          *int64   `json:"forward_date,omitempty"`            // Optional. For forwarded messages, date the original message was sent in Unix time
	IsTopicMessage       *bool    `json:"is_topic_message,omitempty"`        // Optional. True, if the message is sent to a forum topic
	IsAutomaticForward   *bool    `json:"is_automatic_forward,omitempty"`    // Optional. True, if the message is a channel post that was automatically forwarded to the connected discussion group
	ReplyToMessage       *Message `json:"reply_to_message,omitempty"`        // Optional. For replies, the original message. Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
	ViaBot               *User    `json:"via_bot,omitempty"`                 // Optional. Bot through which the message was sent
	EditDate             *int64   `json:"edit_date,omitempty"`               // Optional. Date the message was last edited in Unix time
	HasProtectedContent  *bool    `json:"has_protected_content,omitempty"`   // Optional. True, if the message can't be forwarded
	MediaGroupId         *string  `json:"media_group_id,omitempty"`          // Optional. The unique identifier of a media message group this message belongs to
	AuthorSignature      *string  `json:"author_signature,omitempty"`        // Optional. Signature of the post author for messages in channels, or the custom title of an anonymous group administrator

	ChatId *int64 `json:"chat_id,omitempty"` // Conversation the message belongs to

	Text      *string          `json:"text,omitempty"`       // Optional. For sending messages.
	Entities  []*MessageEntity `json:"entities,omitempty"`   // Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
	Animation *Animation       `json:"animation,omitempty"`  // Optional. Message is an animation, information about the animation. For backward compatibility, when this field is set, the document field will also be set
	Audio     *Audio           `json:"audio,omitempty"`      // Optional. Message is an audio file, information about the file
	Document  *Document        `json:"document,omitempty"`   // Optional. Message is a general file, information about the file
	Photo     []*PhotoSize     `json:"photo,omitempty"`      // Optional. Message is a photo, available sizes of the photo
	Sticker   *Sticker         `json:"sticker,omitempty"`    // Optional. Message is a sticker, information about the sticker
	Video     *Video           `json:"video,omitempty"`      // Optional. Message is a video, information about the video
	VideoNote *VideoNote       `json:"video_note,omitempty"` // Optional. Message is a video note, information about the video message
	// voice 	Voice // Optional. Message is a voice message, information about the file
	// Caption string // Optional. Caption for the animation, audio, document, photo, video or voice
	// caption_entities Array of MessageEntity // Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
	// contact 	Contact // Optional. Message is a shared contact, information about the contact
	// dice 	Dice // Optional. Message is a dice with random value
	// game 	Game // Optional. Message is a game, information about the game. More about games »
	// poll 	Poll // Optional. Message is a native poll, information about the poll
	// venue 	Venue // Optional. Message is a venue, information about the venue. For backward compatibility, when this field is set, the location field will also be set
}
