package entities

type Sticker struct {
	FileId           *string       `json:"fileId"`                     // Identifier for this file, which can be used to download or reuse the file
	FileUniqueId     *string       `json:"fileUniqueId"`               // Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
	StickerType      *string       `json:"stickerType"`                // Type of the sticker, currently one of “regular”, “mask”, “custom_emoji”. The type of the sticker is independent from its format, which is determined by the fields is_animated and is_video.
	Width            *int64        `json:"width"`                      // Sticker width
	Height           *int64        `json:"height"`                     // Sticker height
	IsAnimated       *bool         `json:"isAnimated"`                 // True, if the sticker is animated
	IsVideo          *bool         `json:"isVideo"`                    // True, if the sticker is a video sticker
	Thumbnail        *PhotoSize    `json:"thumbnail,omitempty"`        // Optional. Sticker thumbnail in the .WEBP or .JPG format
	Emoji            *string       `json:"emoji,omitempty"`            // Optional. Emoji associated with the sticker
	SetName          *string       `json:"setName,omitempty"`          // Optional. Name of the sticker set to which the sticker belongs
	PremiumAnimation *File         `json:"premiumAnimation,omitempty"` // Optional. For premium regular stickers, premium animation for the sticker
	MaskPosition     *MaskPosition `json:"maskPosition,omitempty"`     // Optional. For mask stickers, the position where the mask should be placed
	CustomEmojiId    *string       `json:"customEmojiId,omitempty"`    // Optional. For custom emoji stickers, unique identifier of the custom emoji
	NeedsRepainting  *bool         `json:"needsRepainting,omitempty"`  // Optional. True, if the sticker must be repainted to a text color in messages, the color of the Telegram Premium badge in emoji status, white color on chat photos, or another appropriate color in other places
	FileSize         *int64        `json:"fileSize,omitempty"`         // Optional. File size in bytes
}
