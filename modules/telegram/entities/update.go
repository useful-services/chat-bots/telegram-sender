package entities

type Update struct {
	UpdateId *int64   `json:"update_id"`
	Msg      *Message `json:"message"`
}
