package interfaces

type Logger interface {
	OK(args ...any)
	Error(args ...any)
	Warn(args ...any)
	Info(args ...any)
	Debug(args ...any)
	Trace(args ...any)
}
