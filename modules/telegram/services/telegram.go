package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/useful-services/chat-bots/telegram-sender/modules/telegram/entities"
	"gitlab.com/useful-services/chat-bots/telegram-sender/modules/telegram/interfaces"
	"io"
	"net/http"
)

type TelegramService struct {
	url   string
	token string
	host  string

	httpClient *http.Client
	logger     interfaces.Logger
}

func NewTelegramService(url, token, host string, logger interfaces.Logger) *TelegramService {
	logger.Debug("url:", url, "token:", token, "host:", host)

	telegramService := &TelegramService{
		url:   url,
		token: token,
		host:  host,

		httpClient: http.DefaultClient,
		logger:     logger,
	}

	if err := telegramService.setWebhook(host); err != nil {
		logger.Error(err)
		return nil
	}

	return telegramService
}

func (t *TelegramService) Stop() {
	err := t.setWebhook("")
	if err != nil {
		t.logger.Error(err)
	}
}

func (t *TelegramService) setWebhook(host string) error {
	body, err := json.Marshal(&entities.Webhook{Url: &host})
	if err != nil {
		return err
	}

	t.logger.Debug(string(body))
	t.logger.Debug(fmt.Sprintf(t.url, t.token, "setWebhook"))

	resp, err := http.Post(fmt.Sprintf(t.url, t.token, "setWebhook"), "application/json", bytes.NewReader(body))
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	t.logger.Debug(resp.Status, string(body))
	return nil
}

func (t *TelegramService) Send() error {
	return nil
}

func (t *TelegramService) GetUpdates() ([]*entities.Update, error) {
	return nil, nil
}
