package main

import (
	"gitlab.com/useful-services/chat-bots/telegram-sender/pkg/main_process"
)

func main() {
	_ = main_process.Process()
}
